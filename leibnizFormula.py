import numpy as np
import matplotlib.pyplot as plt
import os

#Removing previous resault files
def remove():
    if os.path.exists('iteration_resaults.txt'):
        os.remove('iteration_resaults.txt')
        os.remove('final_resaults.txt')

remove()

#Leibniz formula for calculating PI
absPi = 3.14159
error = 0.00005 
piList = []

def leibnizFormula():
    iteration = 0
    n = 3
    pi = 4

    while  abs(pi - absPi) > error:
        if iteration == 0:
            pi = pi
            piList.append(pi)
            iteration += 1
        elif (iteration % 2 != 0):
            pi = pi - (4/n)
            piList.append(pi)
            n += 2 
            iteration += 1
        else:
            pi = pi + (4/n)
            piList.append(pi)
            n += 2 
            iteration += 1
        print('Iteration:', iteration, '  PI:', pi)
    return pi, iteration, piList

pi, iteration, piList = leibnizFormula()

print('__________________________________________' + '\n')
print('PI: ', pi)
print("Error: ", abs(pi - absPi))
print('Number of iterations: ', iteration)

#Writing resaults to files
iteration_resaults_file = open('iteration_resaults.txt', 'w')
iteration_resaults_file.write('Iterations' + ',' + 'Pi values' + '\n')

k = range(0, iteration)
for i in k:
    g = str(piList[i])
    iteration_resaults_file.write(str(i + 1) + ',' + str(g) + '\n')
iteration_resaults_file.close()

final_resaults_file = open('final_resaults.txt', 'w')
final_resaults_file.write('PI: ' + str(pi) + '\n' + 'Error: ' + str(error) + '\n' + 'Number of iterations: ' + str(iteration))
final_resaults_file.close()

#Ploting resaults
x = np.arange(0, iteration) 
plt.plot(x, piList)
plt.title("Convergence graph")
plt.xlabel("Number of iterations")
plt.ylabel("Values of PI")
plt.grid()
plt.show()



